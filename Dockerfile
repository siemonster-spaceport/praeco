#==========================
#   Building Image
#==========================
FROM node:14-alpine3.14 AS build

WORKDIR /tmp/nginx/praeco
COPY . .
RUN apk add --update --no-cache \
		build-base \
		python3 \
		pkgconfig \
		cairo-dev \
		jpeg-dev \
		pango-dev \
		giflib-dev && \
	npm install && \
	npm run build

#==========================
#   Working Image
#==========================
FROM alpine:3.10

ENV CONFD_VERSION 0.16.0

COPY --from=build /tmp/nginx/praeco/dist /var/www/html
COPY ./entrypoint.sh /entrypoint.sh
COPY ./confd /etc/confd
COPY ./nginx_config/nginx.conf /etc/nginx/nginx.conf
COPY ./public/praeco.config.json /var/www/html/praeco.config.json

RUN true && \
		apk add --update --no-cache \
			bash \
			vim \
			nano \
			nginx \
			curl \
			jq && \
		# install confd
		curl -sSL https://github.com/kelseyhightower/confd/releases/download/v${CONFD_VERSION}/confd-${CONFD_VERSION}-linux-amd64 -o /usr/local/bin/confd && \
		chmod +x /usr/local/bin/confd && \
		# update required rights
		chmod -R 777 /var/tmp/nginx && \
		# create required dirs
		mkdir -p /etc/nginx/conf.d/

EXPOSE 8080
WORKDIR /var/www/html
ENTRYPOINT ["/entrypoint.sh"]
CMD ["nginx", "-g", "daemon off;"]
