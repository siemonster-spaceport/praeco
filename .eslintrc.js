module.exports = {
  root: true,
  env: {
    node: true,
    mocha: true
  },
  extends: ['plugin:vue/recommended', '@vue/airbnb'],
  rules: {
    'arrow-body-style': 'off',
    'semi': 'off',
    'no-unused-vars': 'off',
    'object-curly-newline': 'off',
    'indent': 'off',
    'operator-linebreak': 'off',
    'prefer-template': 'off',
    'object-shorthand': 'off',
    'no-mixed-operators': 'off',
    'no-else-return': 'off',
    'no-lonely-if': 'off',
    'vars-on-top': 'off',
    'no-var': 'off',
    'no-multiple-empty-lines': 'off',
    'keyword-spacing': 'off',
    'prefer-arrow-callback': 'off',
    'no-prototype-builtins': 'off',
    'no-trailing-spaces': 'off',
    'padded-blocks': 'off',
    'no-case-declarations': 'off',
    quotes: 'off',
    'no-underscore-dangle': 'off',
    'no-empty': 'off',
    radix: 'off',
    'no-cond-assign': 'off',
    'no-plusplus': 'off',
    'default-case': 'off',
    'no-labels': 'off',
    'no-restricted-syntax': 'off',
    'consistent-return': 'off',
    'func-names': 'off',
    'arrow-parens': 'off',
    camelcase: 'off',
    'no-console': 'off',
    'no-alert': 'off',
    'prefer-const': 'off',
    'comma-dangle': 'off',
    'prefer-destructuring': 'off',
    'space-before-function-paren': 'off',
    'no-new': 'off',
    'max-len': 'off',
    'vue/require-v-for-key': 'off',
    'vue/require-prop-types': 'off',
    'vue/max-attributes-per-line': 'off',
    'import/prefer-default-export': 'off',
    'vue/no-template-shadow': 'off',
    'import/no-cycle': 'off',
    'vue/return-in-computed-property': 'off',
    'import/extensions': [
      'off',
      'always',
      {
        js: 'never',
        vue: 'never'
      }
    ],
    'no-param-reassign': 'off',
    'import/no-extraneous-dependencies': [
      'error',
      {
        optionalDependencies: ['test/unit/index.js']
      }
    ],
    'no-debugger': process.env.NODE_ENV === 'production' ? 'error' : 'off',
    'vue/html-closing-bracket-newline': 'off'
  },
  parserOptions: {
    parser: 'babel-eslint'
  }
};
