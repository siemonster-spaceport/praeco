#!/bin/bash

export ELASTALERT_HOST=${ELASTALERT_HOST:-praeco-elastalert}
ELASTALERT_STATUS=${ELASTALERT_STATUS:-READY}

function wait_for_elastalert_status {
    while [ "$(curl -k -s ${1}/status |jq -r '.status')" != "${2}" ]; do
        echo "Waiting status ${2} for ${1} ..."
        sleep 5
    done
}

/usr/local/bin/confd -backend="env" -confdir="/etc/confd" -onetime

wait_for_elastalert_status "http://${ELASTALERT_HOST}:3030" "${ELASTALERT_STATUS}"
echo "${ELASTALERT_HOST} is alive"

exec "$@"
