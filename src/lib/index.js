export function baseName(path) {
  return path.split('/').reverse()[0]
}
