import axios from 'axios';
import networkError from '../lib/networkError.js';

export default {
  namespaced: true,
  state: {
    version: '',
    status: '',
    siem_status: '',
    thehiveObservableTypes: [],
    tenants: []
  },
  mutations: {
    FETCHED_VERSION(state, payload) {
      state.version = payload.version;
    },

    FETCHED_STATUS(state, payload) {
      state.status = payload.status;
      if (payload.siem_status) {
        state.siem_status = payload.siem_status;
      }
    },

    FETCHED_THEHIVE_OBSERVABLE_TYPES(state, payload) {
      state.thehiveObservableTypes = payload;
    },

    FETCHED_TENANTS(state, payload) {
      state.tenants = payload;
    }
  },
  actions: {
    async fetchVersion({ commit }) {
      try {
        let res = await axios.get('/api');
        commit('FETCHED_VERSION', res.data);
      } catch (error) {
        networkError(error);
      }
    },

    async fetchStatus({ commit }) {
      try {
        let res = await axios.get('/api/status');
        commit('FETCHED_STATUS', res.data);
      } catch (error) {
        networkError(error);
      }
    },

    async fetchTheHiveObservableTypes({ commit }) {
      try {
        let res = await axios.get('/api/config/thehive/observable_types');
        commit('FETCHED_THEHIVE_OBSERVABLE_TYPES', res.data);
      } catch (error) {
        networkError(error, 'TheHive fetch observables error');
      }
    },

    async fetchTenants({ commit }) {
      try {
        let res = await axios.get('/api/config/tenants');
        commit('FETCHED_TENANTS', res.data);
      } catch (error) {
        networkError(error);
      }
    }
  }
};
